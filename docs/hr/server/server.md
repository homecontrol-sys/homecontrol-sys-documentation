# Server 
<i>Raspbery PI koji sadrži:</i>
* <u>Ubuntu linux OS</u>
* <u>PostgreSQL DBMS</u>
* <u>REST API aplikacija</u>  
<i>Zadužen za poslovnu logiku</i>

	- sadrzi .NET core backend, autentikacija i autorizacija
	- sadrzi postgreSQL relacijsku bazu
	- sadrzi multimediju (slike, zvuk i sl.)
	- sadrzi voice recognition software, jedan od sljedecih(Jasper, Raspberry Pi Voice Recognition by Oscar Liang, Raspberry Pi Voice Control by Steven Hickson) za direktno upravljanje
	  preko mikrofona na Raspberry Pi uredjaju
	- mora moci izvrsiti naredbe na OS-u koje mu je korisnik zadao

* <u>Daemon upravljačka aplikacija</u>  
<i>Zadužena za upravljanje GPIO dijelom Raspbery PI </i>