### <b>Arhitektura:</b> 
* Arhitektura je klijent-server
	* Klijent
		* React Native mobilna apliklacija
	* Server, Raspberry Pi na Ubuntu linux OS-u.
		* PostgreSQL DBMS, baza podataka
		* .NET core REST API, poslovna logika
		* CommandExecutor Aplikacija, upravitelj Raspberry GPIO sučeljem
* Komunikacija između klijenta i servera vrši se bežicno putem WiFi tehnologije.

<i>![Više detalja o arhitekturi...](../../images/diagrams-net/systemArchitecture.png)</i>