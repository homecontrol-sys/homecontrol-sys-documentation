# homecontrol-sys-documentation

## <b>HR</b>
### <b>Zahtijevi:</b>
- Sustav podržava tri vrste korisnika administratora, superusera i usera.
- Sustav ima opciju konfiguracije i integracije u kućnu mrežu.
- Sustav ima mogućnost upravljanjem kontrole pristupa.
- Sustav može izvršiti naredbe korisnika putem glasa i/ili preko opcija izbornika koji se šalju s mobilnog uredjaja(React Native mobilne aplikacije) 
  na mikroprocesor(Raspberry Pi).
- Mikroprcesor može obraditi naredbe poslovnom logikom implementranoj u .NET Core REST API aplikaciji, a izvršiti ih putem Python daemon aplikacije.
- Sustav ima više svojih podsustava: rasvijete, obavijesti, pristupa...
- Sustav omogućuje upravljanje svojim podsustavima(adresiranje elemenata rasvijete, upravljanje pristupa ostalih korisnika i sl.).

---

### <b>Opis sustava:</b> 
<i>Arhitektura je klijent-server gdje je React Native mobilna apliklacija klijent, a Raspberry Pi server s .NET core backendom i CommandExecutor aplikacijom na Ubuntu linux OS-u.</i><br>
<i>Komunikacija se vrši bežicno putem WiFi tehnologije.</i>

---

### <b>Funkcionalnosti:</b>
* Konfiguracija sustava
* Kontrola pristupa
* Upravljanje pristupom
* Upravljanje sustavima

---

### <b>Arhitektura:</b> 
* Arhitektura je klijent-server
	* Klijent
		* React Native mobilna apliklacija
	* Server, Raspberry Pi na Ubuntu linux OS-u.
		* PostgreSQL DBMS, baza podataka
		* .NET core REST API, poslovna logika
		* CommandExecutor Aplikacija, upravitelj Raspberry GPIO sučeljem
* Komunikacija između klijenta i servera vrši se bežicno putem WiFi tehnologije.

![klijent-poslužotelj arhitektura](./images/clientServer.png)

<i>[Više detalja o arhitekturi...](./docs/architecture.md)</i>

### Klijent
<i>Klijent aplikacija</i>
* React Native mobilna aplikacija: 

<i>[Više detalja o klijentu...](./docs/hr/client/client.md)</i>
	
	
### Server 
<i>Raspbery PI - Ubuntu linux distro OS:</i>
* PostgreSQL DBMS
* REST API aplikacija  
		<i>Zadužen za poslovnu logiku</i>
* Daemon upravljačka aplikacija  
		<i>Zadužena za upravljanje GPIO dijelom Raspbery PI </i>

<i>[Više detalja o serveru...](./docs/hr/server/server.md)</i>

---

### <b>Alati:</b>

### Verzioniranje:
* [Git](https://git-scm.com/downloads) - VCS
* [GitLab](https://gitlab.com/) - git web UI
* [Git-Fork](https://git-fork.com/) - git desktop UI

### Modeliranje:
* [diagrams.net](https://app.diagrams.net/)
* [PlantUML](https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml) - VSC dodatak  
	<i>Requrements</i>  
	* [Java](http://www.graphviz.org/download/) : Platform for PlantUML 	running  
	* [Graphviz](http://www.graphviz.org/download/) : PlantUML requires it to calculate positions in diagram

	<i>Documentation</i>  
	* [PlantUml documentation](https://plantuml.com/)

### Razvoj:
* [Visual Studio Code](https://code.visualstudio.com/) - text editor za pisanje koda
* [DBeaver](https://dbeaver.io/download/) - desktop UI za bazu podataka 
* [FileZilla](https://filezilla-project.org/) - FTP klijent za prijenos
* [VirtualBox](https://www.virtualbox.org/) - za staging, simulaciju realnog okruženja, linux OS-a
* [Etcher](https://www.balena.io/etcher/) - za kreiranje SD image datoteke s UbuntuCore OS-om
* [Postman](https://www.postman.com/) - za testiranje API-ja
* [Android Studio](https://developer.android.com/studio) - za korištenje njegovog Emulatora za Android uređaje 
	* [Emulator]() - za testiranje mobilne Android aplikacije na računalu u virtualnom okruženju
	* [Expo Go](https://play.google.com/store/apps/details?id=host.exp.exponent&hl=en_US&gl=US) - za testiranje mobilne Android aplikacije na fizičkom uređaju u realnom okruženju
* [XCode]() - za korištenje njegovog Simulatora za iOS uređaje  
	* [Simulator]() - za testiranje mobilne iOS aplikacije

<i>[Više detalja o alatima razvoja...](./docs/hr/tools.md)</i>

---

### <b>Tehnologije:</b>
### Baza podataka 
* PostgreSQL

### Daemon aplikacija 
* Python

### REST API
* .NET Core 5.0.

### Mobilni klijent  
* Node.js  
	* NPM package manager  
	  * React Native  

---

### <b>Postavljanje razvojnog okruženja:</b>
* Mobilna klijent aplikacija
	* Instalacija Node.js za korištenje NPM paket menadžera
	* Instalacija React Native 'Expo' cli paketa za upravljanje React Native razvojnim okruženjem 
	* Instalacija Android Studio SDK za korištenje Android Emulatora
		 * Postavljanje ANDROID_HOME environment varijable unutar OS-a, default path: 
		 
		 * Postavljanje JAVA_HOME environment varijable unutar OS-a, default path: C:\Users\andjelo\AppData\Local\Android\jre\jre
		 * Postavljanje AVD(Android Virtual Device) Emulatora
		 * Omogučavanje virtalizacije u BIOS-u

<i>[Više detalja o postavljanju razvojnog okruženja...](./docs/hr/devEnvironmentSetup.md)</i>