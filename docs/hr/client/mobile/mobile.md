# Mobilni klijent
### React Native Cross Platform aplikacija
<br>

![Arhitektura mobilnog klijenta](../../../images/export/mobileClinetArchitecture.png)

## Zahtijevi razvojnog okruženja
* Node.js >=12.19.0  
<i>Sadrži NPM paket menadžer za potrebne knjižnice</i>


* 'Expo' knjižnica  
  <i>Sadrži React Native razvojno okruženje</i>  
  > npm install --global expo-cli

---




### Virtual Device Software
<i><b>Virtual Device</b> is software that imitates behavior of a combination of <u>Hardware</u> and <u>operating system</u>.  </i>

* <b>Android Emulator with React Native</b>


* <b>XCode iOS Simulator with React Native</b>
